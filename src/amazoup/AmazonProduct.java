/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amazoup;



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.sql.Connection;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author jedic
 */
class AmazonProduct implements ProductInformation {

    @Override
    public String scrapeASINcode(String resultLink){
        String subString = resultLink.substring(resultLink.lastIndexOf("dp/") + 3, resultLink.indexOf("/ref"));
        System.out.println("The ASIN code is: " + subString);
        return subString;
        
    }
    
    @Override
    public String scrapeManufacturer(Document html) {
        Element productManufacturer = html.select("div#olpProductByline.a-section.a-spacing-mini").first();
        System.out.println("the manufacturer is: " + productManufacturer.text());
        return productManufacturer.text();
    }
   
    @Override
    public String scrapeMinPrice(Document html) {
        Element minPrice = html.select("span.a-size-large.a-color-price.olpOfferPrice.a-text-bold").first();
        System.out.println("the minimum price: " + minPrice.text());
        return minPrice.text();
    }
    
    @Override
    public String scrapeMaxPrice(Document html) {
        Element maxPrice = html.select("span.a-size-large.a-color-price.olpOfferPrice.a-text-bold").last();
        System.out.println("the maximum price: " + maxPrice.text());
        return maxPrice.text();
    }

    @Override
    public String createURL(String barcode) {
        String url = "https://www.amazon.co.uk/s?k=" + barcode + "&ref=nb_sb_noss";
        return url;
    }
    
    @Override
    public String scrapeMinDeliveryCost(Document html) {
        Element transportationCost = html.select("p.olpShippingInfo").first();
        System.out.println("the transportation cost is: " + transportationCost.text());
        return transportationCost.text();
    }
  
    @Override
    public String scrapeSellerName(Document html) {
        Element sellerName = (Element) html.select("h3.a-spacing-none.olpSellerName").first();
        
        System.out.println("Seller: " + sellerName.text());
        return sellerName.text();
    }   

    @Override
    public String scrapeLink(Document html)   {
        String productLink;
        try{
            productLink = html.select("a.a-link-normal.a-text-normal").first().attr("abs:href");
            return productLink;
        }catch(java.lang.NullPointerException ex){
            productLink = "invalid";
            
            return productLink;
        } 
    }
  
    @Override
    public Document connectToSite(String url) {
        Document productListing = null;
        try {
             
             productListing = Jsoup.connect(url).ignoreHttpErrors(true).userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.102 Safari/537.36")
            .header("cookie", "incap_ses_436_255598=\"F9EQLjSoHvMNHRxAq38wDOj6SOfVJ0QOUYqB/XpwC33c4wxiZ6RAjFPs0o/pUb3c6T7Ho2+ocfUkHmD8/iN4TK3rTr6j0VB/BTcORDXLgVqCEP6RB3deDUBgRhCL5gPyySAblpdZStjkPVjGCwIYiKhHgilk5k3MVCwAmutcSMpV0oNTzt8rw/HcBhziAYn0RmKmHagfvDbW4NZHU3VGsw==\"")
            .timeout(0).get();
            
            return productListing; 
            
        } catch (IOException ex) {
            Logger.getLogger(AmazonProduct.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
            return null;
        }
 
    }
    
    @Override
    public String scrapeDescription(Document html) {
        Element productManufacturer = html.select("h1.a-size-large.a-spacing-none").first();
        System.out.println("the Desctription is: " + productManufacturer.text());
        return productManufacturer.text();
    }

    @Override
    public String createListingLink(String asinCode) {
        
        String link = "https://www.amazon.co.uk/gp/offer-listing/" + asinCode;
        System.out.println("ASIN CODE: "+asinCode);
        
        return link;
    }


    @Override
    public int scrapeNumberOfPages(Document html) {
        int totalNumberOfPages;
        Elements numberOfPages = null;
        
        while (numberOfPages != null){
        numberOfPages = html.select("ul.a-pagination");
        
            String lastPageNumber = numberOfPages.last().text();
            String subString = lastPageNumber.substring(lastPageNumber.lastIndexOf(" ") - 1, lastPageNumber.indexOf(" Next→"));   
            totalNumberOfPages = Integer.parseInt(subString);
            return totalNumberOfPages;}
       
            System.out.println("There is only one page with total");
        
      return 0;
    }
    

    @Override
    public String createLastPageLink(int totalNumberOfPages, String resultLink) {
        int indexInt = (totalNumberOfPages-1)*10;
        String strIndex = String.valueOf(indexInt);
        
        String lastPageLink = resultLink+"/ref=olp_page_3?ie=UTF8&f_all=true&startIndex="+strIndex;
        
        
        return lastPageLink;
    }

    @Override
    public String scrapeNumberOfListings(Document html) {
       Element numberOfListings = html.select("span.olp-padding-right").first();
       String number = numberOfListings.text();
       System.out.println("There are totally " + number + " Listings");
       return number;
    }

    @Override
    public String getBarcodes() {
        
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        int returnVal = chooser.showOpenDialog(null);
        String fileDirectory = chooser.getSelectedFile().getAbsolutePath();
        System.out.println(fileDirectory);
        
   
        
        
        return chooser.getSelectedFile().getAbsolutePath();
    }

    @Override
    public void readExcel(String fileDirectory, java.sql.Statement myStmt) {
        
        
        File file = new File(fileDirectory);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AmazonProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        XSSFWorkbook workbook=null;
        try {
            workbook = new XSSFWorkbook(fis);
        } catch (IOException ex) {
            Logger.getLogger(AmazonProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            
                     
            XSSFSheet sheet = workbook.getSheetAt(0);
            XSSFRow row;
            XSSFCell cell;
            Iterator rows = sheet.rowIterator();
            
            for (int rowIndex = 0; rowIndex <= sheet.getLastRowNum(); rowIndex++) {
                row = sheet.getRow(rowIndex);
                
                if (row != null) {
                    cell = row.getCell(5);
                    if (cell.getStringCellValue().equals("") != true) {
                        boolean executeUpdate = myStmt.execute("INSERT INTO product (barcode) SELECT "+cell.getStringCellValue()+" WHERE NOT EXISTS (SELECT barcode FROM product WHERE barcode =" +cell.getStringCellValue() +");");
                        System.out.println(cell.getStringCellValue());
                    
                }
            }
        
        }
            
            System.out.println("done motherfucker");
       
        } catch (SQLException ex) {
            Logger.getLogger(AmazonProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        
 
    }

    @Override
    public String scrapeImageUrl(Document html) {
        Element imageUrl = html.select("img.s-image").first();
       
        System.out.println("Image Url: " + imageUrl.attr("abs:src"));
        
        return imageUrl.attr("abs:src");
    }
    


    @Override
    public java.sql.Connection connectToDb() {
       try {
            String driverName = "org.gjt.mm.mysql.Driver";
            Class.forName(driverName);
            String username = "laptop";
            String pass = "password";
            
            
            java.sql.Connection conn = (java.sql.Connection) DriverManager.getConnection("jdbc:mysql://192.168.1.6:3306/amazon",username,pass);
            return conn;

        } catch (SQLException | ClassNotFoundException ex) {
            Logger.getLogger(AmazonProduct.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }

    @Override
    public ArrayList retrieveFromDb(Statement myStmt) {
       
        try{
            ArrayList<String> inner = new ArrayList<>();
            String query = "SELECT * FROM product";
            ResultSet rs = myStmt.executeQuery(query);
            
            while(rs.next()){
                inner.add(rs.getString("barcode"));
               }
            
            return inner;
            
        } catch (SQLException ex) {
            Logger.getLogger(AmazonProduct.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("problem in RetrieveFromDB");
        }
        return null;
    }

    @Override
    public void updateDB(java.sql.Connection conn, String description, String manufacturer, String barcode, String minPrice, String maxPrice, String deliveryCost, String seller) {
        java.sql.Timestamp date = new java.sql.Timestamp(new java.util.Date().getTime());
        String query = "UPDATE product SET description = ?, manufacturer = ?, "
                + "minPrice = ?, maxPrice = ?, deliveryCost = ?, seller = ?, updated = ? "
                + "where barcode = ?";
        
        try {
            PreparedStatement ps = conn.prepareStatement(query);
            ps.setString(1, description);
            ps.setString(2, manufacturer);
            ps.setString(3, minPrice);
            ps.setString(4, maxPrice );
            ps.setString(5, deliveryCost );
            ps.setString(6, seller );
            ps.setTimestamp(7, date);
            ps.setString(8, barcode);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AmazonProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    @Override
    public String retrieveASINCode(Statement myStmt, String barcode) {
        try {
            String query = "SELECT code FROM product WHERE barcode = '"+barcode+"';";
            
            
            ResultSet rs = myStmt.executeQuery(query);
            rs.next();
            String result = rs.getString("code");
            System.out.println(result);
            
            return rs.getString("code");    
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            return null;
        }
    }

    @Override
    public int getHTTPresponse(String listingUrl) {
        Response response = null;
        
        try {
             System.setProperty("http.proxyHost", "119.82.253.210");
             System.setProperty("http.proxyPort", "54134");   
             response = Jsoup.connect(listingUrl).ignoreHttpErrors(true).userAgent("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.102 Safari/537.36")
            .header("cookie", "incap_ses_436_255598=\"F9EQLjSoHvMNHRxAq38wDOj6SOfVJ0QOUYqB/XpwC33c4wxiZ6RAjFPs0o/pUb3c6T7Ho2+ocfUkHmD8/iN4TK3rTr6j0VB/BTcORDXLgVqCEP6RB3deDUBgRhCL5gPyySAblpdZStjkPVjGCwIYiKhHgilk5k3MVCwAmutcSMpV0oNTzt8rw/HcBhziAYn0RmKmHagfvDbW4NZHU3VGsw==\"")
            .timeout(0).execute();
            System.out.println(response.statusCode());
            return response.statusCode(); 
        } catch (IOException ex) {
            Logger.getLogger(AmazonProduct.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    @Override
    public void deleteBarcodeDb(String asinCode, java.sql.Statement myStmt) {
        try {
            String query = "Delete FROM product WHERE code = '"+asinCode+"';";
            myStmt.execute(query);
            } catch (SQLException ex) {
            System.out.println(ex.toString());
            
        }}

    @Override
    public String retrieveListingLink(Statement myStmt, String barcode) {
         try {
            String query = "SELECT listingUrl FROM product WHERE barcode = '"+barcode+"';";
            
            ResultSet rs = myStmt.executeQuery(query);
            rs.next();
            String result = rs.getString("listingUrl");
            
            
            return rs.getString("listingUrl");    
        } catch (SQLException ex) {
            System.out.println(ex.toString());
            return null;
        }
    }

    

    @Override
    public String retriveTimeStamp(Statement myStmt, String barcode) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    }

