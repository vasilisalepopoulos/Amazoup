/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amazoup;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import org.jsoup.nodes.Document;

/**
 * This class splits the process into two parts. The barcode and ASIN phase. If we are not aware of the ASIN code we run the barcode phase. 
 * But if we have scraped the ASIN Code on a previous instance then we should proceed with the ASIN phase. That way we minimize the number 
 * of connection to Amazon.
 * @author jedic
 * 
 */
class Phase extends AmazonProduct{
   java.sql.Connection connection = connectToDb("root", "password", "jdbc:mysql://localhost:3306/amazon");
   java.sql.Statement myStmt;
   
   /**
    * This phase is used only when we try to scrape product information for the first time
    * 
    * @param barcode
    */
   public void startBarcodePhase(String barcode) throws SQLException{
        String query;
        String searchUrl;
        String listingUrl;
        String asinCode;
        String resultLink;
        int httpResponse = 0;
        
        System.out.println("Barcode Phase commensee");
        
        query = "UPDATE product SET"
              + "code = ?, "
              + "listingUrl = ? "
              + "WHERE barcode = ?";
        PreparedStatement preparedStmt = connection.prepareStatement(query);
        
        
        searchUrl = createURL(barcode);
        Document resultHtml = connectToSite(searchUrl);
        
        
        resultLink = scrapeLink(resultHtml);
        if(resultLink.equals("invalid")){return;}
        asinCode = scrapeASINcode(resultLink);
        listingUrl = createListingLink(asinCode);
        
        
        preparedStmt.setString(1, asinCode);
        preparedStmt.setString(2, listingUrl);
        preparedStmt.setString(3, barcode);
        preparedStmt.execute();
        
        
   }
   
   /**
    * This phase is used mainly to update the product information    * 
    * @param barcode
    */
   
   public void startAsinPhase(String barcode) throws SQLException{
       Document resultHtml;
       String description;
       String manufacturer;
       String minPrice;
       String maxPrice;
       String minDeliveryCost;
       String seller;
       String searchUrl;
       
       
       System.out.println("ASIN phase commence");
       Statement myStmt = connection.createStatement();
       String listingUrl = retrieveListingLink(myStmt, barcode);
       String asinCode = retrieveASINCode(myStmt, barcode);
       
       
       int httpResponse = getHTTPresponse(listingUrl);
       
       if (httpResponse == 200){
           resultHtml = connectToSite(listingUrl);
           description = scrapeDescription(resultHtml);
           manufacturer = scrapeManufacturer(resultHtml);           
           minPrice = scrapeMinPrice(resultHtml);
           minDeliveryCost = scrapeMinDeliveryCost(resultHtml);
           seller = scrapeSellerName(resultHtml);
      
           int totalNumberOfPages = scrapeNumberOfPages(resultHtml);
           
           /* 
                Checks if there are multiple pages of the listing, in order to find the information of the listing with the maximum price
           */
           
           if (totalNumberOfPages != 0){
               searchUrl = createLastPageLink(totalNumberOfPages,listingUrl);
               resultHtml = connectToSite(searchUrl);
               maxPrice = scrapeMaxPrice(resultHtml);
               updateDB(connection, description, manufacturer, barcode, minPrice, maxPrice, minDeliveryCost, seller);
               connection.commit();
           }else if (totalNumberOfPages == 0){
               searchUrl = createLastPageLink(totalNumberOfPages,listingUrl);
               resultHtml = connectToSite(searchUrl);
               maxPrice = scrapeMaxPrice(resultHtml);
               updateDB(connection, description, manufacturer, barcode, minPrice, maxPrice, minDeliveryCost, seller);
               
           }
       }else if(httpResponse == 404){
           deleteBarcodeDb(asinCode, myStmt);
       }
              
   }
   
}
