/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amazoup;

import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;


/**
 * A collection of methods for database use
 * @author Volpym
 */
public interface Database {
     /**
     *  Uses JFileChooser to get the directory of the spreadsheet with the product barcodes
     * @return 
    */
    public String getFileDirectory();
    
    
     /**
     *  Retrieves all the barcodes from the database and returns an ArrayList
     * @param myStmt
     * @return 
    */
    public ArrayList retrieveFromDb (java.sql.Statement myStmt);
    
    
     /**
     *  Retrieves the ASIN code from the database that corresponds to the given barcode
     * @param myStmt
     * @param barcode
     * @return 
    */
    public String retrieveASINCode (java.sql.Statement myStmt, String barcode);
    
    
    /**
     * Connects to the Database 
     * @param username
     * @param password
     * @param database
     * @return
     */
    public java.sql.Connection connectToDb(String username, String password, String database);
    

    /**
     * Updates product's information 
     * 
     * @param conn
     * @param description
     * @param manufacturer
     * @param barcode
     * @param minPrice
     * @param maxPrice
     * @param deliveryCost
     * @param seller
     */
    public void updateDB( java.sql.Connection conn, String description, String manufacturer, String barcode, String minPrice, String maxPrice, String deliveryCost, String seller);
    

    /**
     * Deletes the barcode from the list if it's not valid
     * @param asinCode
     * @param myStmt
     */
    public void deleteBarcodeDb(String asinCode,Statement myStmt);
    

    /**
     * Retrieves the Listing URL of a product 
     * @param myStmt
     * @param barcode
     * @return
     */
    public String retrieveListingLink(Statement myStmt, String barcode);
    

    /**
     * Retrieves the Time Stamp of the latest update
     * @param myStmt
     * @param barcode
     * @return
     */
    public LocalDate retrieveTimeStamp (Statement myStmt, String barcode);
    

    /**
     * Compares the timeStamp with current day/time
     * @param retievedDate
     * @return
     */
    public boolean compareDates (LocalDate retievedDate);
    
    
}
